import express from 'express';
import controller from '../Controllers/ProjectController'

const router = express.Router();

router.get('/listAppointments', controller.listAppointments);
router.get('/getAppointment', controller.getAppointment);
router.post('/addAppointment', controller.addAppointment);
router.post('/deleteAppointment/', controller.deleteAppointment);
router.post('/updateAppointment', controller.updateAppointment);
//router.get('/data', controller.data);

module.exports = router;