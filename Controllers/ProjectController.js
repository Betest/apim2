import Appointment from '../Models/ProjectModel';
let controller = {
    addAppointment: async (req, res) =>{
        console.log(req.body);
        const {firstName, lastName, ident, date, city, neighborhood, mobile, dateAppointment} = req.body;
        const newAppointment = new Appointment({firstName,lastName,ident,date,city,neighborhood,mobile,dateAppointment});
        await newAppointment.save();
        return res.status(200).json({
            response: "Appointment added successfully"
        });
    },
    getAppointment: async (req,res) =>{
        const id = req.query.id;
        const appointment = await Appointment.findById({_id: id});
        return res.status(200).json({appointment});
    },
    listAppointments: async (req,res) =>{
        const appointments = await Appointment.find({});
        return res.status(200).json({appointments});
    },
    updateAppointment: async (req,res) =>{
        const {_id,firstName, lastName, ident, date, city, neighborhood, mobile, dateAppointment} = req.body;
        await Appointment.findByIdAndUpdate(_id,{firstName, lastName, ident, date, city, neighborhood, mobile, dateAppointment});
        return res.status(200).json({
            response: "Appointment updated successfully"
        });
    },
    deleteAppointment: async (req,res) =>{
        console.log(req.body);
        const {id} = req.body;
        Appointment.findByIdAndRemove({_id: id})
        return res.status(200).json({
            response: "Appointment deleted successfully"
        });
    },
}

module.exports = controller;