'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _ProjectController = require('../Controllers/ProjectController');

var _ProjectController2 = _interopRequireDefault(_ProjectController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.get('/listAppointments', _ProjectController2.default.listAppointments);
router.get('/getAppointment', _ProjectController2.default.getAppointment);
router.post('/addAppointment', _ProjectController2.default.addAppointment);
router.post('/deleteAppointment/:id', _ProjectController2.default.deleteAppointment);
router.post('/updateAppointment', _ProjectController2.default.updateAppointment);
//router.get('/data', controller.data);

module.exports = router;