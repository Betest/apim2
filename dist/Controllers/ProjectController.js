"use strict";

var _ProjectModel = require("../Models/ProjectModel");

var _ProjectModel2 = _interopRequireDefault(_ProjectModel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var controller = {
    addAppointment: async function addAppointment(req, res) {
        console.log(req.body);
        var _req$body = req.body,
            firstName = _req$body.firstName,
            lastName = _req$body.lastName,
            ident = _req$body.ident,
            date = _req$body.date,
            city = _req$body.city,
            neighborhood = _req$body.neighborhood,
            mobile = _req$body.mobile,
            dateAppointment = _req$body.dateAppointment;

        var newAppointment = new _ProjectModel2.default({ firstName: firstName, lastName: lastName, ident: ident, date: date, city: city, neighborhood: neighborhood, mobile: mobile, dateAppointment: dateAppointment });
        await newAppointment.save();
        return res.status(200).json({
            response: "Appointment added successfully"
        });
    },
    getAppointment: async function getAppointment(req, res) {
        var id = req.query.id;
        //console.log(name);
        var appointment = await _ProjectModel2.default.findById({ _id: id });
        return res.status(200).json({ appointment: appointment });
    },
    listAppointments: async function listAppointments(req, res) {
        var appointments = await _ProjectModel2.default.find({});
        return res.status(200).json({ appointments: appointments });
    },
    updateAppointment: async function updateAppointment(req, res) {
        var _req$body2 = req.body,
            _id = _req$body2._id,
            firstName = _req$body2.firstName,
            lastName = _req$body2.lastName,
            ident = _req$body2.ident,
            date = _req$body2.date,
            city = _req$body2.city,
            neighborhood = _req$body2.neighborhood,
            mobile = _req$body2.mobile,
            dateAppointment = _req$body2.dateAppointment;

        await _ProjectModel2.default.findByIdAndUpdate(id, { firstName: firstName, lastName: lastName, ident: ident, date: date, city: city, neighborhood: neighborhood, mobile: mobile, dateAppointment: dateAppointment });
        return res.status(200).json({
            response: "Appointment updated successfully"
        });
    },
    deleteAppointment: async function deleteAppointment(req, res) {
        console.log(req.params.id);
        var id = req.params.id.id;

        await _ProjectModel2.default.findByIdAndRemove({ _id: req.params.id });
        return res.status(200).json({
            response: "Appointment deleted successfully"
        });
    }
};

module.exports = controller;