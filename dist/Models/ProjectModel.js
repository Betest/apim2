"use strict";

var _mongoose = require("mongoose");

//const Schema = mongoose.Schema;
//const 
//const {Schema, model} = require('mongoose');

var AppointmentSchema = new _mongoose.Schema({
    firstName: { type: String },
    lastName: { type: String },
    ident: { type: Number },
    date: { type: String },
    city: { type: String },
    neighborhood: { type: String },
    mobile: { type: Number },
    dateAppointment: { type: String }

});

module.exports = (0, _mongoose.model)("Appointment", AppointmentSchema);