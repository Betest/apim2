import {Schema, model} from 'mongoose';
//const Schema = mongoose.Schema;
//const 
//const {Schema, model} = require('mongoose');

const AppointmentSchema = new Schema({
    firstName: {type: String},
    lastName: {type: String},
    ident: {type: Number},
    date: {type: String},
    city: {type: String},
    neighborhood: {type: String},
    mobile: {type: Number},
    dateAppointment: {type: String}

});

module.exports = model("Appointment",AppointmentSchema);